﻿#pragma warning disable 618
using UnityEngine;
using UnityEditor;

// ReSharper disable once CheckNamespace
namespace BuildAutomation
{
    public class BuildAutomationPreferences : MonoBehaviour
    {
        // Have we loaded the prefs yet
        private static bool prefsLoaded = false;

        // The Preferences
        public static bool boolExportCompliance = false;
        //public static bool boolAllowBitcode = false;
        public static bool boolAutonumberBuilds = true;
        public static bool boolShowMeTheBuild = false;

        // Add preferences section named "Build Automation" to the Preferences Window
        [PreferenceItem("Build Automation")]

        public static void PreferencesGUI()
        {
            // Load the preferences
            if (!prefsLoaded)
            {
                boolExportCompliance = EditorPrefs.GetBool("ExportCompliance", true);
                boolAutonumberBuilds = EditorPrefs.GetBool("AutonumberBuilds", false);
                boolShowMeTheBuild = EditorPrefs.GetBool("ShowMeTheBuild", false);
                prefsLoaded = true;
            }

            // Preferences GUI
            boolExportCompliance = EditorGUILayout.Toggle("Set iOS export compliance flag", boolExportCompliance);
            boolAutonumberBuilds = EditorGUILayout.Toggle("Automatically renumber builds", boolAutonumberBuilds);
            boolShowMeTheBuild = EditorGUILayout.Toggle("Open the target folder afterward", boolShowMeTheBuild);

            // Save the preferences
            if (GUI.changed)
            {
                EditorPrefs.SetBool("ExportCompliance", boolExportCompliance);
                EditorPrefs.SetBool("AutonumberBuilds", boolAutonumberBuilds);
                EditorPrefs.SetBool("ShowMeTheBuild", boolShowMeTheBuild);
            }
        }
    }
}
