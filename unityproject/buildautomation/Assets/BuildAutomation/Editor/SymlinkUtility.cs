﻿using UnityEditor;
using UnityEngine;
using System.IO;
using System.Linq;
using System.Diagnostics;

// ReSharper disable once CheckNamespace
namespace Parabox
{
    /**
	 *	An editor utility for easily creating symlinks in your project.
	 *
	 *	Adds a Menu item under `Assets/Create/Folder (Symlink)`, and
	 *	draws a small indicator in the Project view for folders that are
	 *	symlinks.
	 */
    [InitializeOnLoad]
    public static class SymlinkUtility
    {

        private static bool shouldRun
        {
            get
            {
                //don't do when running in a headless CI or commandline
                if (System.Environment.CommandLine.Contains("batchmode") ||
                    System.Environment.CommandLine.Contains("headless") ||
                    System.Environment.CommandLine.Contains("nographics"))
                    return false;

                //don't do while in Play mode in Unity IDE
                return !EditorApplication.isPlaying;
            }
        }

        // FileAttributes that match a junction folder.
        const FileAttributes FOLDER_SYMLINK_ATTRIBS = FileAttributes.Directory | FileAttributes.ReparsePoint;

        // Style used to draw the symlink indicator in the project view.
        private static GUIStyle _symlinkMarkerStyle;
        private static GUIStyle symlinkMarkerStyle =>
            _symlinkMarkerStyle ?? (_symlinkMarkerStyle = new GUIStyle(EditorStyles.label)
            {
                normal = {textColor = new Color(.2f, .8f, .2f, .8f)}, alignment = TextAnchor.MiddleRight
            });

        /*
		 *	Static constructor subscribes to projectWindowItemOnGUI delegate.
		 */
        static SymlinkUtility()
        {
            if (shouldRun)
                EditorApplication.projectWindowItemOnGUI += OnProjectWindowItemGUI;
        }

        /*
		 *	Draw a little indicator if folder is a symlink
		 */
        private static void OnProjectWindowItemGUI(string guid, Rect r)
        {
            try
            {
                var path = AssetDatabase.GUIDToAssetPath(guid);

                if (string.IsNullOrEmpty(path)) return;
                var attribs = File.GetAttributes(path);

                if ((attribs & FOLDER_SYMLINK_ATTRIBS) == FOLDER_SYMLINK_ATTRIBS)
                    GUI.Label(r, "<=>", symlinkMarkerStyle);
            }
            catch (System.Exception e)
            {
                UnityEngine.Debug.LogException(e);
            }
        }

        /*
		 *	Add a menu item in the Assets/Create category to add symlinks to directories.
		 */
        [MenuItem("Assets/Create/Folder (Symlink)", false, 20)]
        private static void DoTheSymlink()
        {
            UnityEngine.Debug.Log("DoTheSymlink");
            var sourceFolderPath = EditorUtility.OpenFolderPanel("Select Folder Source", "", "");

            if (sourceFolderPath.Contains(Application.dataPath))
            {
                UnityEngine.Debug.LogWarning("Cannot create a symlink to folder in your project!");
                return;
            }

            // Cancelled dialog
            if (string.IsNullOrEmpty(sourceFolderPath))
                return;

            var sourceFolderName = sourceFolderPath.Split('/', '\\').LastOrDefault();

            if (string.IsNullOrEmpty(sourceFolderName))
            {
                UnityEngine.Debug.LogWarning("Couldn't deduce the folder name?");
                return;
            }

            var uobject = Selection.activeObject;

            var targetPath = uobject != null ? AssetDatabase.GetAssetPath(uobject) : "";

            if (string.IsNullOrEmpty(targetPath))
                targetPath = "Assets";

            var attribs = File.GetAttributes(targetPath);

            if ((attribs & FileAttributes.Directory) != FileAttributes.Directory)
                targetPath = Path.GetDirectoryName(targetPath);

            targetPath = $"{targetPath}/{sourceFolderName}";

            if (Directory.Exists(targetPath))
            {
                UnityEngine.Debug.LogWarning(
                    $"A folder already exists at this location, aborting link.\n{sourceFolderPath} -> {targetPath}");
                return;
            }

#if UNITY_EDITOR_WIN
			Process cmd = Process.Start("CMD.exe", string.Format("/C mklink /J \"{0}\" \"{1}\"", targetPath, sourceFolderPath));
			cmd.WaitForExit();
#elif UNITY_EDITOR_OSX || UNITY_EDITOR_LINUX
            // ln [-Ffhinsv] source_file [target_file]
            // ln [-Ffhinsv] source_file...target_dir

            UnityEngine.Debug.Log("ln -s {sourceFolderPath} {targetPath}");
            Process cmd = Process.Start("ln", $"-s {sourceFolderPath} {targetPath}");
            cmd?.WaitForExit();

#endif

            UnityEngine.Debug.Log($"Created symlink: {targetPath} <-- {sourceFolderPath}");

            AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
        }
    }
}
