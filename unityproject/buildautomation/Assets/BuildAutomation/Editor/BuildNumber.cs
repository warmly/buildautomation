﻿using System;
using UnityEditor;
using UnityEngine;

namespace BuildAutomation
{
    public static class BuildNumber
    {
        //TODO could add a "verbose" flag to the whole class for debugging
        public static string GetForTarget(BuildTarget buildTarget)
        {
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (buildTarget)
            {
                case BuildTarget.iOS:
                    return PlayerSettings.iOS.buildNumber;
                case BuildTarget.Android:
                    return PlayerSettings.bundleVersion;
                case BuildTarget.StandaloneOSX:
                    return PlayerSettings.macOS.buildNumber;
                default:
                    //do nothing
                    Debug.LogWarning($"BuildNumber not implemented for {buildTarget}");
                    return string.Empty;
            }
        }

        public static void SetForTarget(BuildTarget buildTarget, string newValue)
        {
            Debug.Log("BuildNumber.SetForTarget " + buildTarget + " " + newValue);
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (buildTarget)
            {
                case BuildTarget.iOS:
                    PlayerSettings.iOS.buildNumber = newValue;
                    break;
                case BuildTarget.Android:
                    PlayerSettings.bundleVersion = newValue;
                    break;
                case BuildTarget.StandaloneOSX:
                    PlayerSettings.macOS.buildNumber = newValue;
                    break;
                case BuildTarget.WebGL:
                    //PlayerSettings.companyName?
                    //PlayerSettings.productGUID?
                    //PlayerSettings.productName? - maybe put build# in here
                    break;
                default:
                    //do nothing
                    Debug.LogWarning($"BuildNumber not implemented for {buildTarget}");
                    break;
            }
        }

        public static string Get() => GetForTarget(EditorUserBuildSettings.activeBuildTarget);

        public static void Set(string newValue) => SetForTarget(EditorUserBuildSettings.activeBuildTarget, newValue);

        public static string Timestamp => DateTime.Now.ToString("yyyyMMddHHmm");

        public static string FromTimestamp(string prefix = "") => prefix + Timestamp;

        public static string IncrementingBy(long by = 1, BuildTarget buildTarget = BuildTarget.iOS)
        {
            string ret = string.Empty;
            if (!long.TryParse(GetForTarget(buildTarget), out long result)) return ret;
            result = result + by;
            ret = result.ToString();
            return ret;
        }

        public static void SetByIncrementing(long by = 1, BuildTarget buildTarget = BuildTarget.iOS)
        {
            SetForTarget(buildTarget, IncrementingBy(by, buildTarget));
        }

        public static void SetFromTimestamp(BuildTarget buildTarget)
        {
            SetForTarget(buildTarget, Timestamp);
        }

        [MenuItem("File/Build/Configure/Set/iOS/BuildNumberByIncrementing")]
        public static void SetiOSByIncrementing() { SetByIncrementing(/*1, BuildTarget.iOS*/); }

        [MenuItem("File/Build/Configure/Set/iOS/BuildNumberFromTimestamp")]
        public static void SetiOSFromTimestamp() { SetFromTimestamp(BuildTarget.iOS); }

        public static class Test
        {
            [MenuItem("File/Build/Configure/Test/iOS/BuildNumberByIncrementing")]
            public static void Test_iOSByIncrementing()
            {
                Debug.Log("before: " + PlayerSettings.iOS.buildNumber);
                Debug.Log("after: " + IncrementingBy(/*1, BuildTarget.iOS*/));
            }

            [MenuItem("File/Build/Configure/Test/iOS/BuildNumberFromTimestamp")]
            public static void Test_iOSFromTimestamp()
            {
                Debug.Log("before: " + PlayerSettings.iOS.buildNumber);
                Debug.Log("after: " + FromTimestamp());
            }

        }
    }
}
