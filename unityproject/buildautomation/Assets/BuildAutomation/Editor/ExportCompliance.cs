﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
#if UNITY_IPHONE
using UnityEditor.iOS.Xcode;
#endif

namespace BuildAutomation
{
    public static class ExportCompliance
    {
        [PostProcessBuild(998)]
        public static void OnPostProcessBuild(BuildTarget buildTarget, string path)
        {
            if (buildTarget != BuildTarget.iOS)
                return;

            if (!EditorPrefs.GetBool("ExportCompliance"))
                return;

#if UNITY_IPHONE

            string projectPath = PBXProject.GetPBXProjectPath(path);
            PBXProject pbxProject = new PBXProject();
            pbxProject.ReadFromFile(projectPath);

            var plistPath = System.IO.Path.Combine(path, "Info.plist");
            var plist = new PlistDocument();
            plist.ReadFromFile(plistPath);

            string exportcomplianceKey = "ITSAppUsesNonExemptEncryption";
            plist.root.SetBoolean(exportcomplianceKey, false);
            plist.WriteToFile(plistPath);

            Debug.LogWarning("Set export compliance Info.plist flag as required by Apple");
#endif
        }
    }
}