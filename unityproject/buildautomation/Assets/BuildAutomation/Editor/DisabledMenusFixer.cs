﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;

// from https://forum.unity3d.com/threads/menu-items-disabled-after-playing-in-editor.443568/
// NOTE: requires Unity >5.5

namespace BuildAutomation
{
    public static class DisabledMenusFixer
    {
        private const int MIN_TIME = 100;

        private static bool shouldRun
        {
            get
            {
                //don't do this in most recent Unity versions
#if UNITY_2019_1_OR_NEWER
                return false;
#endif
                //don't do when running in a headless CI or commandline
#pragma warning disable 162
                if (System.Environment.CommandLine.Contains("batchmode") ||
                    System.Environment.CommandLine.Contains("headless") ||
                    System.Environment.CommandLine.Contains("nographics"))
                    return false;

                //don't do while in Play mode in Unity IDE
                if (EditorApplication.isPlayingOrWillChangePlaymode)
                {
                    return false;
                }

                //don't do if this Unity IDE has only recently started up
                if (EditorApplication.timeSinceStartup < MIN_TIME)
                {
                    return false;
                }
                
                return true;
#pragma warning restore 162
            }
        }

        [InitializeOnLoadMethod]
        private static void SetupCallback()
        {
            if (!shouldRun) return;


#if UNITY_2017_2_OR_NEWER
            EditorApplication.playModeStateChanged -= PlayModeChanged;
            EditorApplication.playModeStateChanged += PlayModeChanged;
        }

        private static void PlayModeChanged(PlayModeStateChange state)
        {
            Fix();
        }
#else
            EditorApplication.playmodeStateChanged -= PlayModeChanged;
            EditorApplication.playmodeStateChanged += PlayModeChanged;
        }

        static void PlayModeChanged()
        {
            Fix();
        }
#endif


        [DidReloadScripts]
        private static void Fix()
        {
            if (!shouldRun) return;
            Debug.Log($"DisabledMenusFixer Fix {EditorApplication.timeSinceStartup}");
            
            var oldSelection = Selection.activeGameObject;
            Selection.activeGameObject = GameObject.CreatePrimitive(PrimitiveType.Cube);

            // Do the trick
            EditorApplication.ExecuteMenuItem("Component/Add...");
            if (EditorWindow.focusedWindow != null)
            {
                EditorWindow.focusedWindow.SendEvent(Event.KeyboardEvent("escape"));
            }

            Object.DestroyImmediate(Selection.activeGameObject);
            Selection.activeGameObject = oldSelection;
        }

    }

}
