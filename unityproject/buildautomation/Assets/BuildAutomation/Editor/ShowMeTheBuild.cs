﻿using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace BuildAutomation
{
    class ShowMeTheBuild : IPostprocessBuildWithReport
    {
        private bool ShouldRun
        {
            get
            {
                //don't do when running in a headless CI or commandline
                if (System.Environment.CommandLine.Contains("batchmode") ||
                    System.Environment.CommandLine.Contains("headless") ||
                    System.Environment.CommandLine.Contains("nographics"))
                {
                    Debug.Log("ShowMeTheBuild: skipping because headless...");
                    return false;
                }

                //don't do while in Play mode in Unity IDE
                return !EditorApplication.isPlaying;
            }
        }

        public int callbackOrder { get { return 9999; } }
        public void OnPostprocessBuild(BuildReport report)
        {
            Debug.Log("ShowMeTheBuild.OnPostprocessBuild for target " + report.summary.platform + " at path " + report.summary.outputPath);

            if (EditorPrefs.GetBool("ShowMeTheBuild") && ShouldRun)
            {
                Menu_File_Build.OpenTargetFolder(report.summary.platform);
            }
        }
    }
}