﻿using System;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace BuildAutomation
{
    public static class Versions
    {
#if UNITY_EDITOR

        public static string GetForTarget(BuildTarget buildTarget)
        {
            switch (buildTarget)
            {
                case BuildTarget.iOS:
                    return Get();
                case BuildTarget.StandaloneOSX:
                    return Get();
                default:
                    return Get();
            }
        }

        public static void SetForTarget(BuildTarget buildTarget, string newValue)
        {
            switch (buildTarget)
            {
                case BuildTarget.iOS:
                    Set(newValue);
                    break;
                case BuildTarget.StandaloneOSX:
                    Set(newValue);
                    break;
                default:
                    Set(newValue);
                    break;
            }
        }

        public static string Get()
        {
            return PlayerSettings.bundleVersion;
        }

        public static void Set(string newValue)
        {
            PlayerSettings.bundleVersion = newValue;
        }

        public static string Prefix
        {
            get
            {
                string ret;
                string bv = Get();
                string lastNumber = bv.Split('.').Last();
                ret = bv.Substring(0, bv.LastIndexOf(lastNumber, StringComparison.Ordinal));
                Debug.LogFormat("prefix: " + ret);
                return ret;
            }
        }

        public static string Suffix
        {
            get
            {
                //skip any prefixes so we can just increment the final number, if any
                return PlayerSettings.bundleVersion.Split('.').Last();
            }
        }

        public static string Timestamp => DateTime.Now.ToString("yyyyMMddHHmm");

        public static string FromTimestamp(string prefix = "")
        {
            string ret = prefix; // e.g. major_dot_minor;

            if (string.IsNullOrEmpty(prefix))
            {
                //use prefix from the current setting, if it has one
                ret += Prefix;
            }

            ret += Timestamp;

            return ret;
        }

        public static string IncrementingBy(long by = 1, bool verbose = false)
        {
            string ret = PlayerSettings.bundleVersion;

            long result;
            if (long.TryParse(Versions.Suffix, out result))
            {
                result += by;
                ret = Versions.Prefix + result;
                if (verbose) Debug.Log("incremented version: " + ret);
            }
            else
            {
                if (verbose) Debug.Log("could not increment version from " + PlayerSettings.bundleVersion);
            }
            return ret;
        }


        [MenuItem("File/Build/Configure/Set/VersionByIncrementing")]
        public static void SetByIncrementing() { Set(IncrementingBy(1, true)); }

        [MenuItem("File/Build/Configure/Set/VersionFromTimestamp0_1")]
        public static void SetFromTimestamp0_1() { Set(FromTimestamp("0.1.")); }

        [MenuItem("File/Build/Configure/Set/VersionFromTimestamp")]
        public static void SetFromTimestamp() { Set(FromTimestamp()); }


        public static class Test
        {

            [MenuItem("File/Build/Configure/Test/VersionByIncrementing")]
            public static void Test_ByIncrementing()
            {
                Debug.Log("before: " + Get());
                Debug.Log("after: " + IncrementingBy());
            }

            [MenuItem("File/Build/Configure/Test/VersionFromTimestamp0_1")]
            public static void Test_FromTimestamp0_1()
            {
                Debug.Log("before: " + Get());
                Debug.Log("after: " + FromTimestamp("0.1."));
            }

            [MenuItem("File/Build/Configure/Test/VersionFromTimestamp")]
            public static void Test_FromTimestamp()
            {
                Debug.Log("before: " + Get());
                Debug.Log("after: " + FromTimestamp());
            }

        }

#endif
    }
}