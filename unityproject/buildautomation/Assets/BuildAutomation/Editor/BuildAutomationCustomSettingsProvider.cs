﻿
// WIP
// see https://forum.unity.com/threads/preferenceitem-is-deprecated-use-settingsprovider-instead.554437/
// and https://github.com/Unity-Technologies/UnityCsReference/blob/master/Editor/Mono/Inspector/GraphicsSettingsInspector.cs


using System.IO;
using System.Linq;
using UnityEditor;

class BuildAutomationSettingsProvider : SettingsProvider
{
    const string k_BuildAutomationSettingsPath = "Resources/BuildAutomationSettings.asset";
    public BuildAutomationSettingsProvider(string path, SettingsScope scope)
        : base(path, scope) {}

    public static bool IsSettingsAvailable()
    {
        return File.Exists(k_BuildAutomationSettingsPath);
    }

    [SettingsProvider]
    public static SettingsProvider CreateBuildAutomationSettingsProvider()
    {
        if (IsSettingsAvailable())
        {
            return new BuildAutomationSettingsProvider("Build Automation", SettingsScope.Project);
        }

        // Settings Asset doesn't exist yet. No need to display anything in the Settings window.
        return null;
    }
}
