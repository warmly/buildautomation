﻿using UnityEditor;

namespace BuildAutomation
{
    public static class Menu_File_Build_AssetBundles
    {
        const string BUNDLE_FOLDER_NAME = "bundles";

        static string GetAssetBundleOutputPath(BuildTarget target)
        {
            return System.IO.Path.Combine(BUNDLE_FOLDER_NAME, target.ToString());
        }

        //Menu_File_Build_AssetBundles
        [MenuItem("File/Build/AssetBundles/iOS")]
        static void GenerateiOSAssetBundleBuild()
        {
            // TODO insert ci logging and integration here
            GenerateAssetBundleBuild(BuildAssetBundleOptions.None, BuildTarget.iOS);
        }

        public static void GenerateAssetBundleBuild(BuildAssetBundleOptions options, BuildTarget target)
        {
            var outputPath = GetAssetBundleOutputPath(target);
            if (!System.IO.Directory.Exists(outputPath))
            {
                System.IO.Directory.CreateDirectory(outputPath);
            }
            BuildPipeline.BuildAssetBundles(outputPath, options, target);
        }
    }
}