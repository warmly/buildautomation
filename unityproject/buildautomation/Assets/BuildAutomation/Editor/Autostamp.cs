﻿using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace BuildAutomation
{
    class Autostamp : IPreprocessBuildWithReport
    {
        public int callbackOrder { get { return 0; } }
        public void OnPreprocessBuild(BuildReport report)
        {
            Debug.Log("Autostamp.OnPreprocessBuild for target " + report.summary.platform + " at path " + report.summary.outputPath);

            if (!EditorPrefs.GetBool("AutonumberBuilds"))
                return;

            if (report.summary.platform != BuildTarget.iOS)
                return;

            //TODO also make this optional from commandline?
            BuildNumber.SetFromTimestamp(report.summary.platform);
        }
    }
}