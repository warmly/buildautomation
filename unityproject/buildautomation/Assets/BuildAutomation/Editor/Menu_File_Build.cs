﻿using UnityEngine;
using UnityEditor;
using System;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace BuildAutomation
{
    public static class Menu_File_Build
    {
        #region Open Target Folder
        [MenuItem("File/Build/Open target folder/iOS")]
        private static void OpenTargetFolderiOS() => OpenTargetFolder(BuildTarget.iOS);
        // Validate the menu item defined by the function above.
        // The menu item will be disabled if this function returns false.
        [MenuItem("File/Build/Open target folder/iOS", true)]
        private static bool ValidateOpenTargetFolderiOS()
        {
            return System.IO.Directory.Exists(targetFolder(BuildTarget.iOS, true));
        }
        [MenuItem("File/Build/Open target folder/MacOS")]
        private static void OpenTargetFolderStandaloneOSX() => OpenTargetFolder(BuildTarget.StandaloneOSX);
        [MenuItem("File/Build/Open target folder/MacOS", true)]
        private static bool ValidateOpenTargetFolderStandaloneOSX()
        {
            return System.IO.Directory.Exists(targetFolder(BuildTarget.StandaloneOSX, true));
        }
        [MenuItem("File/Build/Open target folder/Windows 64-bit")]
        private static void OpenTargetFolderStandaloneWindows64() => OpenTargetFolder(BuildTarget.StandaloneWindows64);
        [MenuItem("File/Build/Open target folder/Windows 64-bit", true)]
        private static bool ValidateOpenTargetFolderStandaloneWindows64()
        {
            return System.IO.Directory.Exists(Application.dataPath.Replace("Assets", "") + targetFolder(BuildTarget.StandaloneWindows64));
        }
        [MenuItem("File/Build/Open target folder/Windows 32-bit")]
        private static void OpenTargetFolderStandaloneWindows() => OpenTargetFolder(BuildTarget.StandaloneWindows);
        [MenuItem("File/Build/Open target folder/Windows 32-bit", true)]
        private static bool ValidateOpenTargetFolderStandaloneWindows()
        {
            return System.IO.Directory.Exists(Application.dataPath.Replace("Assets", "") + targetFolder(BuildTarget.StandaloneWindows));
        }
        [MenuItem("File/Build/Open target folder/Android")]
        private static void OpenTargetFolderAndroid() => OpenTargetFolder(BuildTarget.Android);
        [MenuItem("File/Build/Open target folder/Android", true)]
        private static bool ValidateOpenTargetFolderAndroid()
        {
            return System.IO.Directory.Exists(targetFolder(BuildTarget.Android, true)) ||
                   System.IO.File.Exists(targetFolder(BuildTarget.Android, true));
        }
        [MenuItem("File/Build/Open target folder/WebGL")]
        private static void OpenTargetFolderWebGL() => OpenTargetFolder(BuildTarget.WebGL);
        [MenuItem("File/Build/Open target folder/WebGL", true)]
        private static bool ValidateOpenTargetFolderWebGL()
        {
            return System.IO.Directory.Exists(Application.dataPath.Replace("Assets", "") + targetFolder(BuildTarget.WebGL));
        }

        public static void OpenTargetFolder(BuildTarget buildTarget)
        {
            //Debug.Log("OpenTargetFolder " + buildTarget);
            var folder = targetFolder(buildTarget, true);
            if (buildTarget == BuildTarget.StandaloneOSX) folder = folder + "/..";
            OpenInFileBrowser.Open(folder);
        }
        #endregion

        //note: I like to separate out the different platforms underneath a parent folder, as we do here:
        private static string targetFolder(BuildTarget buildTarget, bool fullPath = false)
        {
            string productName = PlayerSettings.productName;//TODO make some EditorPrefs for these
            var ret = System.IO.Path.Combine("target", buildTarget.ToString());
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (buildTarget)
            {
                case BuildTarget.StandaloneOSX:
                    ret = System.IO.Path.Combine(ret, productName + ".app");
                    break;
                case BuildTarget.Android:
                    ret = System.IO.Path.Combine(ret, productName + ".apk");
                    break;
                case BuildTarget.WebGL:
                    ret = System.IO.Path.Combine(ret, productName);
                    break;
                case BuildTarget.StandaloneWindows:
                case BuildTarget.StandaloneWindows64:
                    ret = System.IO.Path.Combine(ret, productName + ".exe");
                    break;
                case BuildTarget.Lumin:
                    break;
                // ReSharper disable RedundantCaseLabel
                case BuildTarget.iOS:
                    break;
                //case BuildTarget.StandaloneLinux:
                case BuildTarget.WSAPlayer:
                case BuildTarget.StandaloneLinux64:
                //case BuildTarget.StandaloneLinuxUniversal:
                case BuildTarget.PS4:
                case BuildTarget.XboxOne:
                case BuildTarget.tvOS:
                case BuildTarget.Switch:
                case BuildTarget.NoTarget:
                default:
                    throw new ArgumentOutOfRangeException(nameof(buildTarget), buildTarget, null);
            }

            if (fullPath)
            {
                ret = Application.dataPath.Replace("Assets", "") + ret;
                //Debug.Log($"ret = {ret}");
            }
            return ret;
        }

        private static string[] GetBuildScenes()
        {
            return (from e in EditorBuildSettings.scenes where e != null where e.enabled select e.path).ToArray();
        }

        [MenuItem("File/Build/Build now!/iOS")]
        private static void GenerateiOS() => GenerateBuild(BuildTarget.iOS);

        [MenuItem("File/Build/Build now!/MacOS")]
        private static void GenerateStandaloneOSX() => GenerateBuild(BuildTarget.StandaloneOSX);

        [MenuItem("File/Build/Build now!/Windows 64-bit")]
        private static void GenerateStandaloneWindows64() => GenerateBuild(BuildTarget.StandaloneWindows64);

        [MenuItem("File/Build/Build now!/Windows 32-bit")]
        private static void GenerateStandaloneWindows() => GenerateBuild(BuildTarget.StandaloneWindows);

        [MenuItem("File/Build/Build now!/Android")]
        private static void GenerateAndroid() => GenerateBuild(BuildTarget.Android);

        [MenuItem("File/Build/Build now!/WebGL")]
        private static void GenerateWebGL() => GenerateBuild(BuildTarget.WebGL);

        private static void GenerateBuild(BuildTarget buildTarget)
        {
            var startTime = DateTime.Now;
            Debug.Log(DateTime.Now + " BEGIN GenerateBuild " + buildTarget);
            //Debug.Log("Language ->" + CommandLineReader.GetCustomArgument("Language"));
            //Debug.Log("Version ->" + CommandLineReader.GetCustomArgument("Version"));

            //Versions.SetByIncrementing();//TODO make this optional from commandline

            if (EditorPrefs.GetBool("AutonumberBuilds"))
            {
                BuildNumber.SetFromTimestamp(buildTarget);//TODO also make this optional from commandline
            }
            string playerFolder = targetFolder(buildTarget);

            Debug.Log("playerFolder path is " + playerFolder);
            Debug.Log($"Folder exists? {System.IO.Directory.Exists(playerFolder)}");
            Debug.Log($"File exists? {System.IO.File.Exists(playerFolder)}");

            //TODO process setting version and build numbers here
            var isNewProject = !System.IO.Directory.Exists(playerFolder) && !System.IO.File.Exists(playerFolder)
                                                                         //Android non-Eclipse build should never try to append;
                                                                         //make it always a 'new' project
                                                                         || (buildTarget == BuildTarget.Android &&
                                                                              playerFolder.EndsWith(".apk"));

            string buildRootPath = Application.dataPath.Replace("Assets", "") + playerFolder;
            Debug.Log("destination path is " + buildRootPath);

            //if the project has been built previously, append to it:
            if (isNewProject)
            {
                Debug.Log("GenerateBuild() building as new project");

                //special case for current Android builds: force the issue by deleting old build
                if (buildTarget == BuildTarget.Android || buildTarget == BuildTarget.StandaloneWindows64 || buildTarget == BuildTarget.StandaloneWindows)
                {
                    Debug.Log($"Deleting {buildRootPath}");
                    FileUtil.DeleteFileOrDirectory(buildRootPath);
                }
                else
                {
                    Debug.Log($"Creating {buildRootPath}");
                    System.IO.Directory.CreateDirectory(buildRootPath);
                }

                Build(buildTarget);
            }
            else
            {
                Debug.Log("GenerateBuild() building by appending existing project");
                Build(buildTarget, BuildOptions.AcceptExternalModificationsToPlayer);
            }
            var elapsed = DateTime.Now - startTime;
            Debug.Log(DateTime.Now + "  END GenerateBuild " + buildTarget + " elapsed time: " + elapsed);
        }

        public static void Build(BuildTarget buildTarget, BuildOptions buildOptions = BuildOptions.None)
        {
            try
            {
                var buildPlayerOptions = new BuildPlayerOptions
                {
                    assetBundleManifestPath = "",
                    scenes = GetBuildScenes(),
                    locationPathName = targetFolder(buildTarget),
                    target = buildTarget,
                    options = buildOptions | BuildOptions.CompressWithLz4HC
                };
                BuildPipeline.BuildPlayer(buildPlayerOptions);
            }
            catch (Exception e)
            {
                //we'll emit this text for Jenkins plugins to pick up on: TODO there is a better way now
                Debug.LogError("BUILD FAILED: " + e);
                EditorApplication.Exit(-6990);
            }
        }
    }
}
